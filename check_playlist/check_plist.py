import requests
from check_dynamo_db import add_to_dynamodb

playlistId = "PLqHdw3nbXmM-hh9Qk8tVNiEPxMN4G7mYr"
key = "AIzaSyBTb0wBcbgb7q2u-fFP7mPBCHqmNw0GH8k"
part = "snippet"
maxResults = "500"
url = "https://youtube.googleapis.com/youtube/v3/playlistItems?part="+part+"&playlistId="+playlistId+"&key="+key+"&maxResults="+maxResults
yt_prefix = "https://youtube.com/watch?v="

# print(url)

response = requests.get(url)
#print(response.json().keys())

dict_data = response.json()
list_data_items = dict_data.get("items")
# print(list_data_items)

# print(list_data_items[2]["snippet"]["title"])

for i in range(len(list_data_items)):
    video_title = list_data_items[i]["snippet"]["title"]
    videoId = yt_prefix+list_data_items[i]["snippet"]["resourceId"]["videoId"]
    publishedAt = list_data_items[i]["snippet"]["publishedAt"]
    videoOwnerChannelTitle = list_data_items[i]["snippet"]["videoOwnerChannelTitle"]
    print(videoOwnerChannelTitle)
    print(video_title)
    print(videoId)
    print(publishedAt)
    print("--------------------------------------------------------------------------------------")
    add_to_dynamodb(videoId, videoOwnerChannelTitle, video_title, publishedAt)
#Check if DB contains videoId
