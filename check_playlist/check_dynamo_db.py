import boto3

# Get the service resource.
dynamodb = boto3.resource('dynamodb')

# Instantiate a table resource object without actually
# creating a DynamoDB table. Note that the attributes of this table
# are lazy-loaded: a request is not made nor are the attribute
# values populated until the attributes
# on the table resource are accessed or its load() method is called.
table = dynamodb.Table('download-yt')

# Print out some data about the table.
# This will cause a request to be made to DynamoDB and its attribute
# values will be set based on the response.
# print(table.creation_date_time)

def add_to_dynamodb(videoId, videoOwnerChannelTitle, video_title, publishedAt):
    table.put_item(
       Item={
            'videoId':  videoId,
            'videoOwnerChannelTitle': videoOwnerChannelTitle,
            'video_title': video_title,
            'publishedAt': publishedAt,
        }
    )

# add_to_dynamodb("test1", "test2", "test3")
# response = table.get_item(
#     Key={
#         'videoId': 'test',
#     }
# )
# item = response['Item']
# print(item)