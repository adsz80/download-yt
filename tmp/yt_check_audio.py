from pymediainfo import MediaInfo

test_file_1 = "a.mp4"
test_file_2 = "C:/Video/b.mp4"

media_info = MediaInfo.parse(test_file_2)


for track in media_info.tracks:
    if track.track_type == "Video":
        print("Bit rate: {t.bit_rate}, Frame rate: {t.frame_rate}, "
              "Format: {t.format}".format(t=track)
        )
        print("Duration (raw value):", track.duration)
        print("Duration (other values:")
        print(track.other_duration)
    elif track.track_type == "Audio":
        print("Track data:")
        print(track.to_data())

if track.to_data == 0:
    print('No Audio')
else:
    print('Audio is available1')

